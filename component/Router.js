import React from 'react';

import { createAppContainer,createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { View, Text, TabBarIOS } from 'react-native';
import { Icon } from 'native-base';


import Auth from './Auth';
import Login from './Login';
import Beranda from './view/Beranda';
import Formulir from './view/Formulir';
import Perizinan from './view/Perizinan';
import Notif from './view/Notif';


const AppStack = createMaterialBottomTabNavigator(
  {
    Beranda: {
      screen: Beranda,
      path: 'tab-a',
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({focused}) =><Icon name="apps" />,
      })
    },
    Formulir: {
      screen: Formulir,
      path: 'tab-b',
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({focused}) =><Icon name="person" />,
      })
    },
    Perizinan: {
      screen: Perizinan,
      path: 'tab-b',
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({focused}) =><Icon name="camera" />,
      })
    },
    Notif: {
      screen: Notif,
      path: 'tab-b',
      navigationOptions: ({ navigation }) => ({
        tabBarIcon: ({focused}) =><Icon active name="navigate" />,
      })
    }
  },
  {
    initialRouteName: 'Beranda',
    activeColor: '#f0edf6',
    inactiveColor: '#3e2465',
    labeled: false,
    barStyle: { backgroundColor: '#ffff' },
  }
);

 const AuthStack = createStackNavigator(
       { Login: Login }
   );


 export default createAppContainer(createSwitchNavigator(
   {
       AuthLoading: Auth,
       App: AppStack,
       Auth: AuthStack,
       },
       {
         initialRouteName: 'AuthLoading',
       }
   ));
