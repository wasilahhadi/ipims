import { AsyncStorage } from 'react-native';

const session = {
  async saveKey(key, valueToSave) {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(valueToSave));
    } catch (error) {
      console.log('AsyncStorage Error: ' + error);
    }
  },

  async saveEmail(key, valueToSave) {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(valueToSave));
    } catch (error) {
      console.log('AsyncStorage Error: ' + error);
    }
  },

  async Clear(key) {
    try {
      await AsyncStorage.clear();
    } catch (error) {
      console.log('Log Out Error: ' + error);
    }
  }


};

export default session;
