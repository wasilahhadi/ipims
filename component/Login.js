import React, { Component } from 'react';
import { View,
          Text,
          TextInput,
          StyleSheet,
          Button,
          Image,
          Alert } from 'react-native';
import deviceStorage from './DB/session';

export default class Login extends Component<Props> {
  static navigationOptions = {
      header: null
  }
  constructor(props){
      super(props);
      this.state = {email:'', password:'', error:'', loading:false};

  }
  onLoginPress(){
    this.setState({error:'', loading:true});
    fetch('https://sayatawar.com/doLogin', {
      method: 'GET',
    })
      .then(response => response.json())
      .then(json => {
              deviceStorage.saveEmail("email", email);
        this.setState({
          jsonData: json.body,
        });
      })
      .catch(error => {
        console.error(error);
      });
    }


onSignUpPress(){
    this.setState({error:'', loading:true});


}

renderButtonOrLoading(){
    if (this.state.loading) {
        return <Text> Loading </Text>
    }

    return <View>
        <Button
        onPress={this.onLoginPress.bind(this)}
        title= 'Login'/>
    </View>
}
  render() {
    return (
      <View style={styles.container}>


      <View style={styles.formContainer}>

        <TextInput style = {styles.input}
        placeholder='Email'
        value = {this.state.email}
        onChangeText={email => this.setState({email})}
        autoFocus={true}/>

        <TextInput style = {styles.input}
        placeholder='Password'
        value = {this.state.password}
        onChangeText={password => this.setState({password})}
        secureTextEntry={true} />
        <Text>{this.state.error}</Text>
        {this.renderButtonOrLoading()}


    </View>
    </View>
    );
  }
}
const styles = StyleSheet.create({
    container: {
        padding: 20,
        marginTop:150
    },
    input:{
        height: 40,
        backgroundColor: 'rgba(225,225,225,0.2)',
        marginBottom: 10,
        padding: 10,
        color: '#555'
    },
    buttonContainer:{
        backgroundColor: '#2980b6',
        paddingVertical: 15
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    }
});
