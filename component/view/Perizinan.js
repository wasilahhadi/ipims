import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { List, Checkbox , Button} from 'react-native-paper';

export default class Perizinan extends Component<Props> {
  constructor(props){
      super(props);
      this.state = {email:'', password:'', error:'', loading:false};

  }
  renderButtonOrLoading(){
      if (this.state.loading) {
          return <Text> Loading </Text>
      }

  }
  render() {
    return (
      <View style={styles.container}>
      <List.Section title="Status Perizinan">
        <List.Accordion
          title="Nama Dokumen : Formulir Izin Kerja"
        >
          <List.Item title="No Dokumen : PLTU/SPK. 002/XII-2019" />
          <List.Item title="Jenis Pekerjaan : Pengaspalan Jalan" />
          <List.Item title="Lama Pekerjaan : 5 Hari Kerja" />
          <List.Item title="Status : Aktif" />
          <Button mode="contained" onPress={() => console.log('Pressed')}>view</Button>
        </List.Accordion>


        <List.Accordion
          title="Nama Dokumen : Formulir Izin Kerja 2"
          expanded={this.state.expanded}
          onPress={this._handlePress}
        >
        <List.Item title="No Dokumen : PLTU/SPK. 002/XII-2019" />
        <List.Item title="Jenis Pekerjaan : Pengaspalan Jalan2" />
        <List.Item title="Lama Pekerjaan : 5 Hari Kerja2" />
        <List.Item title="Status : Aktif" />
        <Button mode="contained" onPress={() => console.log('Pressed')}>view</Button>
        </List.Accordion>
      </List.Section>
    </View>
    );
  }
}
const styles = StyleSheet.create({
    container: {
        marginTop:2
    }
});
