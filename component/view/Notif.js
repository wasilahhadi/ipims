import React, { Component } from 'react';
import { StyleSheet, View} from 'react-native';
import { ListItem } from 'react-native-elements';

const list = [
  {
    title: 'Dokumen izin masuk area kerja anda sudah disetujui. Selanjutnya silahkan untuk melengkapi dokumen izin kerja',
    icon: 'av-timer'
  },
  {
    title: 'Dokumen izin masuk area kerja anda ditolak !. Silahkan menghubungi atasan anda',
    icon: 'av-timer'
  },
]
export default class Notif extends Component<Props> {

  constructor(props){
      super(props);
      this.state = {email:'', password:'', error:'', loading:false};

  }


renderButtonOrLoading(){
    if (this.state.loading) {
        return <Text> Loading </Text>
    }

}
  render() {
    return (
      <View style={styles.container}>
      <View>
        {
          list.map((item, i) => (
            <ListItem
              key={i}
              title={item.title}
              leftIcon={{ name: item.icon }}
              bottomDivider
              chevron
            />
          ))
        }
      </View>
    </View>
    );
  }
}
const styles = StyleSheet.create({
    container: {
        padding: 2,
        marginTop:2
    }
});
