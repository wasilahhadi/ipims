import React, { Component } from 'react';
import { View,
          Text,
          TextInput,
          StyleSheet,
          Button,
          Image,
          Alert } from 'react-native';
import { List, ListItem, Left, Right, Icon } from 'native-base';
import Timeline from 'react-native-timeline-flatlist';

export default class Beranda extends Component<Props> {

  constructor(props){
      super(props);
      this.data = [
        {time: 'approve', title: 'Mitra Kerja', description: 'The Beginner Archery and Beginner Crossbow course does not require you to bring any equipment, since everything you need will be provided for the course. '},
        {time: 'approve', title: 'User', description: 'Badminton is a racquet sport played using racquets to hit a shuttlecock across a net.'},
        {time: 'approve', title: 'Operator'},
        {time: 'reject', title: 'Pengawas K3', description: 'Team sport played between two teams of eleven players with a spherical ball. ',lineColor:'#009688'},
      ]
      this.state = {email:'', password:'', error:'', loading:false};
  }


renderButtonOrLoading(){
    if (this.state.loading) {
        return <Text> Loading </Text>
    }

}
  render() {
    return (

      <View style={styles.container}>
      <Image source = {{uri:'https://i2.wp.com/www.isomanajemen.com/wp-content/uploads/2019/03/Kepanjangan-Dari-K3.jpg?w=750&ssl=1'}}
      style = {{ width: 400, height: 200 }}
      />
      <List>
        <ListItem noIndent style={{ backgroundColor: "#cde1f9" }}>
          <Left>
            <Text>STATUS DOKUMEN</Text>
          </Left>
        </ListItem>
        <ListItem >
         <Left>
            <Text>NAMA DOKUMEN : IJIN KHUSUS</Text>
          </Left>
        </ListItem>
        <ListItem>
          <Left>
            <Text>No. SPK : 12345</Text>
          </Left>
        </ListItem>
      </List>
        <Timeline
          style={styles.list}
          data={this.data}
          separator={true}
          circleSize={20}
          circleColor='rgb(45,156,219)'
          lineColor='rgb(45,156,219)'
          timeContainerStyle={{minWidth:52, marginTop: 5}}
          timeStyle={{textAlign: 'center', backgroundColor:'#ff9797', color:'white', padding:5, borderRadius:13, overflow: 'hidden'}}
          descriptionStyle={{color:'gray'}}
          options={{
            style:{paddingTop:5}
          }}
        />
      </View>

    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white'
  },
  list: {
    padding :5,
    flex: 1,
    marginTop:20,
  },
});
