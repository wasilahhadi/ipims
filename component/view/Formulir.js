import React, { Component } from 'react';
import { View, StyleSheet ,ScrollView, SafeAreaView} from 'react-native';
import { PricingCard } from 'react-native-elements';

export default class FormIjinKerja extends Component<Props> {

  constructor(props){
      super(props);
      this.state = {email:'', password:'', error:'', loading:false};

  }


renderButtonOrLoading(){
    if (this.state.loading) {
        return <Text> Loading </Text>
    }

}
  render() {
    return (
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
      <View style={styles.container}>
      <PricingCard
        color="#4f9deb"
        price="Form izin masuk area kerja"
        info={['Anda harus melengkapi form ini untuk dapat melanjutkan proses selanjutnya']}
        button={{ title: 'Lengkapi', icon: 'flight-takeoff' }}
      />
      <PricingCard
        color="#4f9deb"
        price="Form izin kerja"
        info={['Anda harus melengkapi form ini untuk dapat melanjutkan proses selanjutnya']}
        button={{ title: 'Lengkapi', icon: 'flight-takeoff' }}
      />
      <PricingCard
        color="#4f9deb"
        price="Form kerja selesai"
        info={['Anda harus melengkapi form ini untuk dapat melanjutkan proses selanjutnya']}
        button={{ title: 'Lengkapi', icon: 'flight-takeoff' }}
      />
      <PricingCard
        color="#4f9deb"
        price="Form izin keluar area kerja"
        info={['Anda harus melengkapi form ini untuk dapat melanjutkan proses selanjutnya']}
        button={{ title: 'Lengkapi', icon: 'flight-takeoff' }}
      />
    </View>
    </ScrollView>
  </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
    container: {
        padding: 2,
        marginTop:2
    }
});
