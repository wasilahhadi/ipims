/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './component/Router';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
